var express = require('express');
var app = express();

var api= require('./routes/api');
var api2 = require('./routes/api2');
app.use('/api',api);
app.use('/apiV2',api2);

app.get('/', (req, res) => {
        res.send('<h1>Pagina principal!</h1>');
    });
// app.get('/', (req, res) => {
//     res.send('<h1>Utilizaste el verbo GET!</h1>');
// });
// app.post('/', (req, res) => {
//     res.send('<h1>Utilizaste el verbo POST!</h1>');
// });
// app.put('/', (req, res) => {
//     res.status(400).send('<h1>Utilizaste el verbo PUT!</h1>');
// });
// app.delete('/', (req, res) => {
//     res.status(500).send('<h1>Utilizaste el verbo DELETE!</h1>');
// });


app.listen(3000,()=>{console.log('Escuchando por el puerto 3000')});