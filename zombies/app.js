var http = require('http');
var path = require('path');
var express = require('express');

var app = express();

app.use(express.static(path.resolve(__dirname,"public")));
app.set("views",path.resolve(__dirname,"views"));
app.set("view engine","ejs");

var publicPath=path.join(__dirname,'img');
app.use('/img',express.static(publicPath));

app.get('/',(request,response)=>response.render('clases'));
app.get('/clases',(request,response)=>response.render('clases'));
app.get('/armas',(request,response)=>response.render('armas'));
app.get('/victimas',(request,response)=>response.render('victimas'));

app.listen(3000,()=>console.log('Escuchando por el puerto 3000'));